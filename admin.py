# Course: CSP1150
# StudentID: 10390807
# StudentName: Damion.Brown
# StudentEmail: DAMIONB@OUR.ECU.EDU.AU

import json
import os


# Gets user input and makes sure it's an int thats not 0.
def inputInt(prompt):
    while True:
        try:  # try, expect a possible exception
            temp = int(input(prompt))
            if temp > 0:
                return temp
            else:
                print("Must be greater than 0")
        except ValueError:
            print("Non-negative whole number required.")  # If something other than a numerical value was entered.


# Gets user input and parses it as a string.
def inputSomething(prompt):
    while True:
        try:  # try, expect a possible exception
            temp = str(input(prompt))
            if temp.strip() is not "":
                return temp
            else:
                print("Must be at least one character.")
        except ValueError:  # This probably shouldn't ever happen and I havn't been able to cuase it to happen so I can't test it.
            print("Input could not be parsed as a String.")
            print("KERNEL PAGE FAULT (ERROR CODE: 2)")


# Saves the changes, opens the file, dumps to it then closes the stream.
def saveChanges(data):
    try:
        with open('data.txt', 'w') as f:
            json.dump(data, f, ensure_ascii=False)
            f.close()
    except Exception:
        raise


# Prints out some game info, neater as a function than in the body.
def printInfo(base, index):
    print("Name: ", base[index]["name"], sep="")
    print("Minimum Players: ", base[index]["max_players"], sep="")
    print("Maximum Players: ", base[index]["max_players"], sep="")
    print("Minimum Age: ", base[index]["min_age"], sep="")
    print("Duration: ", base[index]["duration"], sep="")


# Some nice confirmation logic, defaults as Yes because requires less effort from me during debug.
def confirm(prompt):
    while True:
        try:  # try, expect a possible exception
            prompt += " [Y/n]"
            temp = str(input(prompt))
            if (temp.__contains__("y")) or (temp.strip() is ""):  # if no return [Y/n] return y (true)
                return True
            else:
                return False
        except ValueError:  # again, untestable.
            print("Input could not be parsed.")
            print("KERNEL PAGE FAULT (ERROR CODE: 2)")


# Check if the file exists if it doesn't create an empty one.
def preflight():
    if not os.path.isfile("data.txt"):
        print("data.txt doesn't exist, creating.")
        saveChanges(data)


data = []
preflight()

# Open the file, deserialize it, close the stream.
try:
    with open('data.txt', 'r') as data_file:
        data = json.load(data_file)
        data_file.close()
except Exception as e:
    print("data.json is corrupted")
    raise

# Main loop
try:
    while True:
        print()
        print('Choose [a]dd, [l]ist, [s]earch, [v]iew, [d]elete or [q]uit.')
        choice = inputSomething(
            '> ').split()  # Some cool choice stuff, basicaly depends on how many arguments are submitted
        if len(choice) > 1:
            args = True
        else:
            args = False

        if choice[0] == 'a':
            while True:
                tmp = [0, 1, 2, 3, 4]
                tmp[0] = inputInt("Minimum Players (EG: 2): ")
                tmp[1] = inputInt("Minimum Age (EG: 7): ")
                tmp[2] = inputInt("Maximum Players (EG: 6): ")
                tmp[3] = inputSomething("Name (EG: Chess): ")
                tmp[4] = inputInt("Duration (EG: 60): ")
                if confirm("Is this information correct?"):
                    for rows in data:  # Check if something alrady exists by that name
                        if (str(rows['name']).lower().strip()) == (str(tmp[3]).lower().strip()):
                            print("A game by the name \"", tmp[3], "\" already exists.", sep="")
                            break  # Die if it does
                    data.append({'min_players': tmp[0], 'min_age': tmp[1], 'max_players': tmp[2], 'name': tmp[3],
                                 'duration': tmp[4]})
                    saveChanges(data)  # append the game and save changes.
                    print("Game added.")
                    break
                else:
                    print("Game was not added.")
        elif choice[0] == 'l':
            i = 1
            for rows in data:  # list all the games.
                print(i, ') ', rows['name'], sep="")
                i += 1
            if len(data) == 0:
                print("There are no games to list.")
        elif choice[0] == 's':
            if args:
                search = choice[1]
            else:
                search = inputSomething("Search Term: ")
            i = 1
            found = 0
            for rows in data:  # Search all items to see if they contain the submitted string
                if (str(rows['name']).lower().strip()).__contains__(str(search).lower().strip()):
                    found += 1
                    print(i, ') ', rows['name'], sep="")
                    i += 1
            if len(data) == 0:
                print("There are no games to search.")
            elif found == 0:
                print("No games found.")
        elif choice[0] == 'v':
            if args:
                try:
                    view = int(choice[1]) - 1
                except ValueError:
                    print("View requires an index number.")
                    view = inputInt("Index Number: ") - 1
            else:
                view = inputInt("Index Number: ") - 1
            if len(data) == 0:
                print("There are no games to view.")
            elif view < (len(data)):
                printInfo(data, view)  # Heres that function I described earlier to print game data.
            else:
                print("Index out of range.")
        elif choice[0] == 'd':
            if args:
                try:
                    delete = int(choice[1]) - 1
                except ValueError:
                    print("Delete requires an index number.")
                    delete = inputInt("Index Number: ") - 1
            else:
                delete = inputInt("Index Number: ") - 1
            if len(data) == 0:
                print("There are no games to delete.")
            elif delete < (len(data)):
                printInfo(data, delete)  # Again, print out all the game info and ask for confirmation.
                if confirm("Are you sure you want to delete this entry?"):
                    data.pop(delete)  # Delete it.
                    saveChanges(data)  # Save it.
                    print("Entry deleted.")
                else:
                    print("Entry was not deleted.")
            else:
                print("Index out of range.")
        elif choice[0] == 'q':  # K bye.
            print("Ｇｏｏｄｂｙｅ！")
            exit(0)
        else:
            print("Invalid Choice")
except KeyboardInterrupt:  # Catch ctrl+c and k-bye it.
    print("Ｇｏｏｄｂｙｅ！")
    exit(0)
