# Course: CSP1150
# StudentID: 10390807
# StudentName: Damion.Brown
# StudentEmail: DAMIONB@OUR.ECU.EDU.AU

from tkinter import *
from random import randint
import json


class ProgramGUI:
    def __init__(self):
        self.data = []
        self.root = Tk()  # Create tkinter object

        self.root.title("Game Finder")  # Set the window title

        title = Label(self.root, text="Constraints:", fg="blue", font=("Helvetica", 16))  # Labels (Title/header)
        title.grid(row=0, columnspan=2)  # Using the grid system to manage placement

        label1 = Label(self.root, text="Number of players: ", fg="black", font=("Helvetica", 10))
        label1.grid(row=1, column=0, padx=(10, 0))
        label2 = Label(self.root, text="Time Available (mins): ", fg="black", font=("Helvetica", 10))
        label2.grid(row=2, column=0, padx=(10, 0))
        label3 = Label(self.root, text="Age of youngest player: ", fg="black", font=("Helvetica", 10))
        label3.grid(row=3, column=0, padx=(10, 0))

        self.entry1 = Entry(self.root)  # Entries for user input
        self.entry1.config(width=5)
        self.entry1.grid(row=1, column=1, padx=(0, 10))
        self.entry2 = Entry(self.root)
        self.entry2.config(width=5)
        self.entry2.grid(row=2, column=1, padx=(0, 10))
        self.entry3 = Entry(self.root)
        self.entry3.config(width=5)
        self.entry3.grid(row=3, column=1, padx=(0, 10))

        separator = Frame(height=8)  # some seperators because you said to use them
        separator.grid(row=4)

        button = Button(self.root, text="Choose for me", width=10, command=self.pickRandom)  # A button
        button.grid(row=5, column=0)

        button = Button(self.root, text="Search", width=10, command=self.findGames)  # Another button
        button.grid(row=5, column=1, padx=(0, 10))

        separator = Frame(height=10)
        separator.grid(row=6)

        title = Label(self.root, text="Matching Games:", fg="blue", font=("Helvetica", 16))  # Another title
        title.grid(row=7, columnspan=2)

        self.lab = Label(self.root, text="null", fg="black",
                         font=("Helvetica", 10))  # Label that will contain the game names

        try:
            with open('data.txt', 'r') as data_file:  # open the file
                self.data = json.load(data_file)  # deserialize it w/ json
                data_file.close()  # close it
        except ValueError as e:
            print("data.json is corrupted or unreadable")
            self.root.destroy()  # in event of error destroy because you said to.
            return  # also terminate this function

        self.root.mainloop()  # Enter the mainloop for the GUI.

    def findGames(self):
        tmplist = []
        tmp = ""
        for rows in self.data:
            # If no data is entered in any feild display all games
            if (self.entry1.get() == "") and (self.entry2.get() == "") and (self.entry3.get() == ""):
                tmplist.append(rows['name'])
            # if all the conditions are met show those games.
            # Conditions as per decribed in the assignment PDF.
            if checkInt(self.entry1) and checkInt(self.entry2) and checkInt(self.entry3):
                if (int(rows['max_players'])) >= (int(self.entry1.get())) >= (int(rows['min_players'])):
                    if (int(self.entry2.get())) <= (int(rows['duration'])):
                        if (int(self.entry3.get())) <= (int(rows['min_age'])):
                            tmplist.append(rows['name'])
                            # append the game name to a list
        if len(self.data) == 0:
            # If data has nothing in it say there are no games.
            tmp = "There are no games to search."
        self.lab.destroy()  # delete any text
        for rows in tmplist:
            tmp += rows + "\n"  # go through that temp list and make one string of everything with new lines inbetween
        self.lab = Label(self.root, text=tmp, fg="black", font=("Helvetica", 10))  # Label of that.
        self.lab.grid(row=8, columnspan=2)
        return

    # Literally the same as above expect rather than appending all items on the list with \n it just picks one at random and reutrns just that.,
    def pickRandom(self):
        tmplist = []
        for rows in self.data:
            if (self.entry1.get() == "") and (self.entry2.get() == "") and (self.entry3.get() == ""):
                tmplist.append(rows['name'])
            if checkInt(self.entry1) and checkInt(self.entry2) and checkInt(self.entry3):
                if (int(rows['max_players'])) >= (int(self.entry1.get())) >= (int(rows['min_players'])):
                    if (int(self.entry2.get())) <= (int(rows['duration'])):
                        if (int(self.entry3.get())) <= (int(rows['min_age'])):
                            tmplist.append(rows['name'])
        if len(self.data) == 0:
            tmp = "There are no games to pick from."
        self.lab.destroy()
        self.lab = Label(self.root, text=tmplist[randint(0, len(tmplist) - 1)], fg="black", font=("Helvetica", 10))
        self.lab.grid(row=8, columnspan=2)
        return


# Return true if the text in the entry is an int otherwise return false.
def checkInt(entry):
    try:
        int(entry.get())
        return True
    except ValueError:
        return False


# Create an object of the ProgramGUI class to begin the program.
gui = ProgramGUI()  # Instance the above class.
# Exit when it termiantes.
exit(0)
